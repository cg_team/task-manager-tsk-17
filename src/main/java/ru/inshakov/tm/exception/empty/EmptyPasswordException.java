package ru.inshakov.tm.exception.empty;

public class EmptyPasswordException extends RuntimeException {

    public EmptyPasswordException() {
        super("Error! Id is empty...");
    }

}
