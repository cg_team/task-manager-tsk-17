package ru.inshakov.tm.repository;

import ru.inshakov.tm.api.repository.ITaskRepository;
import ru.inshakov.tm.exception.empty.EmptyIdException;
import ru.inshakov.tm.exception.entity.TaskNotFoundException;
import ru.inshakov.tm.model.Project;
import ru.inshakov.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    public List<Task> findAll(Comparator<Task> comparator) {
        final List<Task> listOfTasks = new ArrayList<>(tasks);
        listOfTasks.sort(comparator);
        return listOfTasks;
    }

    @Override
    public void add(final Task task) {
        tasks.add(task);
    }

    @Override
    public void remove(final Task task) {
        tasks.remove(task);
    }

    @Override
    public void clear() {
        tasks.clear();
    }

    @Override
    public Task findOneById(final String id) {
        for (final Task task: tasks) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task findOneByProjectId(final String projectId) {
        for (final Task task: tasks) {
            if (projectId.equals(task.getProjectId())) return task;
        }
        return null;
    }

    @Override
    public Task removeOneByProjectId(final String projectId) {
        final Task task = findOneByProjectId(projectId);
        if (task == null) throw new TaskNotFoundException();
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeOneById(final String id) {
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        tasks.remove(task);
        return task;
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        return tasks.get(index);
    }

    @Override
    public Task findOneByName(final String name) {
        for(final Task task: tasks) {
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public List<Task> findAllTasksByProjectId(final String projectId) {
        final List<Task> tasksByProjectId = new ArrayList<>();
        for (final Task task: tasks){
            if (projectId.equals(task.getProjectId())) tasksByProjectId.add(task);
        }
        return tasksByProjectId;
    }

    @Override
    public List<Task> removeAllTasksByProjectId(final String projectId) {
        final List<Task> tasksByProjectId = new ArrayList<>();
        for (final Task task: tasks){
            if (projectId.equals(task.getProjectId())) tasksByProjectId.remove(task);
        }
        return tasksByProjectId;
    }

    @Override
    public int size() {
        return tasks.size();
    }

}
