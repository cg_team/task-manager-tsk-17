package ru.inshakov.tm.api.repository;

import ru.inshakov.tm.model.User;

import java.util.List;

public interface IUserRepository {

    List<User> findAll();

    User add(User user);

    User findById(String id);

    User removeUser(User user);

    User findByLogin(String login);

    User removeById(String id);

    User removeByLogin(String login);

}
