package ru.inshakov.tm.api.service;

import ru.inshakov.tm.command.AbstractCommand;
import ru.inshakov.tm.model.Command;

import java.util.Collection;

public interface ICommandService {

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArg(String arg);

    Collection<AbstractCommand> getArguments();

    Collection<AbstractCommand> getCommands();

    void add(AbstractCommand command);

}
