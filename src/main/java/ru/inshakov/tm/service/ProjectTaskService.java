package ru.inshakov.tm.service;

import ru.inshakov.tm.api.repository.IProjectRepository;
import ru.inshakov.tm.api.repository.ITaskRepository;
import ru.inshakov.tm.api.service.IProjectTaskService;
import ru.inshakov.tm.enumerated.Status;
import ru.inshakov.tm.exception.entity.ProjectNotFoundException;
import ru.inshakov.tm.exception.entity.TaskNotFoundException;
import ru.inshakov.tm.model.Project;
import ru.inshakov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private IProjectRepository projectRepository;

    private ITaskRepository taskRepository;

    public ProjectTaskService(IProjectRepository projectRepository, ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAllTasksByProjectId(String projectId) {
        List<Task> tasksByProjectId = taskRepository.findAllTasksByProjectId(projectId);
        return tasksByProjectId;
    }

    public Task defineTaskAndProject (String projectId, String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @Override
    public Task bindTaskByProject(String projectId, String taskId) {
        final Task task = defineTaskAndProject(projectId, taskId);
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskFromProject(String projectId, String taskId) {
        final Task task = defineTaskAndProject(projectId, taskId);
        task.setProjectId(null);
        return task;
    }

    @Override
    public Project removeProjectById(String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        taskRepository.removeAllTasksByProjectId(projectId);
        return projectRepository.removeOneById(projectId);
    }

}
