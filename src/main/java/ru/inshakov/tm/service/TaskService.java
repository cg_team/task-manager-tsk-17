package ru.inshakov.tm.service;

import ru.inshakov.tm.api.repository.ITaskRepository;
import ru.inshakov.tm.api.service.ITaskService;
import ru.inshakov.tm.enumerated.Status;
import ru.inshakov.tm.exception.empty.EmptyIdException;
import ru.inshakov.tm.exception.empty.EmptyNameException;
import ru.inshakov.tm.exception.entity.TaskNotFoundException;
import ru.inshakov.tm.exception.system.IndexIncorrectException;
import ru.inshakov.tm.exception.system.UndefinedErrorException;
import ru.inshakov.tm.model.Task;

import ru.inshakov.tm.util.ValidationUtil;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

import static ru.inshakov.tm.util.ValidationUtil.isEmpty;

public class TaskService implements ITaskService {

    private ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public List<Task> findAll(final Comparator<Task> comparator) {
        if (comparator == null) throw new UndefinedErrorException();
        return taskRepository.findAll(comparator);
    }

    @Override
    public Task add(final String name, final String description) {
        if (isEmpty(name)) throw new EmptyNameException();
        //if (isEmpty(description)) return null;
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
        return task;
    }

    @Override
    public void remove(final Task task) {
        if (task == null) throw new TaskNotFoundException();
        taskRepository.remove(task);
    }

    @Override
    public Task findOneByName(final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        return taskRepository.findOneByName(name);
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        if (ValidationUtil.checkIndex(index, taskRepository.size())) throw new IndexIncorrectException();;
        return taskRepository.findOneByIndex(index);
    }

    @Override
    public void removeOneByIndex(final Integer index) {
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();;
        remove(task);
    }

    @Override
    public void removeOneByName(final String name) {
        final Task task = findOneByName(name);
        if (task == null) throw new TaskNotFoundException();;
        remove(task);
    }

    @Override
    public Task findOneById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();;
        return taskRepository.findOneById(id);
    }

    @Override
    public Task removeOneById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.removeOneById(id);
    }

    @Override
    public Task changeTaskStatusById(String id, Status status) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = findOneById(id);
        if (task == null) return null;
        task.setStatus(status);
        if (status == Status.IN_PROGRESS) task.setDateStart(new Date());
        if (status == Status.COMPLETE) task.setDateFinish(new Date());
        return task;
    }

    @Override
    public Task changeTaskStatusByName(String name, Status status) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByName(name);
        if (task == null) return null;
        task.setStatus(status);
        if (status == Status.IN_PROGRESS) task.setDateStart(new Date());
        if (status == Status.COMPLETE) task.setDateFinish(new Date());
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(Integer index, Status status) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        task.setStatus(status);
        if (status == Status.IN_PROGRESS) task.setDateStart(new Date());
        if (status == Status.COMPLETE) task.setDateFinish(new Date());
        return task;
    }

    @Override
    public Task updateTaskByIndex(final Integer index, final String name, final String description) {
        if (ValidationUtil.checkIndex(index, taskRepository.size())) throw new IndexIncorrectException();
        if (isEmpty(name)) throw new EmptyNameException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateTaskById(final String id, final String name, final String description) {
        if (isEmpty(id)) throw new EmptyIdException();
        if (isEmpty(name)) throw new EmptyNameException();
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task startTaskById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByName(name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task finishTaskById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task finishTaskByIndex(final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task finishTaskByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByName(name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

}
