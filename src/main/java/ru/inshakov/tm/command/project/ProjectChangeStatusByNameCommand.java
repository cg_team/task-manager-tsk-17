package ru.inshakov.tm.command.project;

import ru.inshakov.tm.command.AbstractProjectCommand;
import ru.inshakov.tm.enumerated.Status;
import ru.inshakov.tm.exception.entity.ProjectNotFoundException;
import ru.inshakov.tm.model.Project;
import ru.inshakov.tm.util.TerminalUtil;

import java.util.Arrays;

public class ProjectChangeStatusByNameCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-change-status-by-name";
    }

    @Override
    public String description() {
        return "change project status by name";
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS]");
        System.out.println("ENTER NAME");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Project project = serviceLocator.getProjectService().changeProjectStatusByName(name, status);
        if (project == null) throw new ProjectNotFoundException();
    }

}
