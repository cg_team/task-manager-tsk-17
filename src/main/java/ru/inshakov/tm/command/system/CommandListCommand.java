package ru.inshakov.tm.command.system;

import ru.inshakov.tm.command.AbstractCommand;

import java.util.Collection;

public class CommandListCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "command-list";
    }

    @Override
    public String description() {
        return "Show program commands";
    }

    @Override
    public void execute() {
        System.out.println("[COMMANDS]");
        final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getCommands();
        for (final AbstractCommand command: commands) {
            final String name = command.name();
            if (name == null) continue;
            System.out.println(name);
        }
    }

}
