package ru.inshakov.tm.command.task;

import ru.inshakov.tm.api.service.ICommandService;
import ru.inshakov.tm.api.service.IProjectService;
import ru.inshakov.tm.api.service.IProjectTaskService;
import ru.inshakov.tm.api.service.ITaskService;
import ru.inshakov.tm.command.AbstractTaskCommand;
import ru.inshakov.tm.exception.entity.TaskNotFoundException;
import ru.inshakov.tm.model.Task;
import ru.inshakov.tm.util.TerminalUtil;

public class TaskCreateCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-create";
    }

    @Override
    public String description() {
        return "create new task";
    }

    @Override
    public void execute() {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String description = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().add(name, description);
        if (task == null) throw new TaskNotFoundException();
    }

}
